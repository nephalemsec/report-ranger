---
name: SQL injection vulnerability identified in your application
likelihood: Likely
impact: Severe
suggest_rc:
- "Developer education"
- "System configuration guidelines"
suggest_ar:
- "Developer workshop"
- "System configuration guidelines"
...

### Risk assessment {-}

Your database has probably already been ripped and disclosed to the internet. This vulnerability is easily identified by automated tools and requires little skill or experience to exploit to its full capability.

### Description {-}

There was an SQL injection vulnerability identified in your application in this year, the year {{ date.strftime("%Y") }}. Despite SQL injection being a vulnerability that was first publicised {{ int(date.strftime("%Y")) - 1998 }} years ago in 1998, your application developers apparently have not yet heard of this new vulnerability class that is appearing in systems.

This SQL injection vulnerability was identified in the following URLs:
{{
of.table('
| URL | parameter |
| :---: | :---: |
| www.rrclient.com/vulnerableurl.asp | putsqlhere |
')
}}

### Recommendations {-}

To remove SQL injection from your system, you should use parameterised queries or, alternatively, a scripting language invented within the last ten years that automatically parameterises your queries. This will at least aim the gun away from your foot, although you are seemingly quite intent on shooting your foot.