---
title: Report Ranger testing report
client: Ranging Automation
template: sample
author: Volkis
authoremail: info@volkis.com.au
authormobile: "Mobile Number"
results:
  number:
    - 115
    - 31
    - 18
    - 13
  stage:
    - Email Sent
    - Email Opened
    - Clicked Link
    - Submitted Data
---

{{ new_section() }}

# Charts


{{ fig("Phishing campaign results", px.funnel(x=[115,31,18,13], y=['Email sent','Email opened','Clicked link','Entered data'], labels={'y': ''}, template=plotly_template)) }}

{{
  bar_chart(
    "The number of bars shown so far",
    {'First bar': 1, 'Second bar': 2},
    labels={'index':'', 'value':'Number of bars'},
    height=300
  )
}}

{{ 
  bar_chart("Two categories of data",
    {
      'First category':{'First bar': 1, 'Second bar': 2},
      'Second category':{'First bar': 3, 'Second bar': 4}
    },
    labels={'index':'', 'value':'Number of bars'},
    height=300)
}}

{{ 
  bar_chart("Two categories of data",
    {
      'First category':{'First bar': 1, 'Second bar': 2},
      'Second category':{'First bar': 3, 'Second bar': 4}
    },
    labels={'index':'', 'value':'Number of bars'},
    height=300,
    barmode='group'
    )
}}

{{ bar_chart("The number of bars shown now", {'Third bar': 3, 'Fourth bar': 4}, labels={'index':'', 'value':'Number of bars'}, orientation='h', height=400) }}

{{ fig("Test radar plot", px.bar_polar(pd.DataFrame({'direction':['Information security policies','Organisation of information security','Human resource security','Asset management','Access control', 'Cryptography', 'Physical and environmental security', 'Operations security', "System lifecycle",'Supplier relationships','Incident management', 'Business continuity management','Compliance'], 'value': [1,2,2,3,2,3,4,1,5,2,5,1,2]}), r='value', theta='direction', template=plotly_template)) }}

{{ 
  
  fig("Essential 8 maturity model levels", px.bar_polar(pd.DataFrame(
    {'direction':['Application control','Patch applications','Patch applications', 'Patch operating systems', 'Patch operating systems', 'Patch operating systems','User application hardening','Configure Microsoft Office macro settings','Restrict administrative privileges', 'Multi-factor authentication', 'Regular backups', 'Regular backups', 'Regular backups'],
    'value': [0,1,1,1,1,1,1,0,1,1,1,1,1],
    'maturity': ['0','1','2','1','2','3','1','1','1','1','1','2','3']},
    ), r='value', theta='direction', template=plotly_template, color='maturity', height=350,
    color_discrete_map={'1':'red','2':'orange','3':'green'}
    ),
    update_layout={
      'showlegend': False,
      'polar_radialaxis_dtick': 1,
      'polar_radialaxis_range': [0,3]
    }

  )
 }}

{{
  polar_bar_chart("Essential 8 maturity model levels",
    {
      'Application control':0,
      'Patch applications':2,
      'Patch operating systems':2,
      'User application hardening':1,
      'Configure Microsoft Office macro settings':0,
      'Restrict administrative privileges':0,
      'Multi-factor authentication':3,
      'Regular backups':3
    },
    colors={'1':'#3b3a3d','2':'#001c32','3':'#5e9ca8'},
    range=[0,3],
    height=500
  )
}}

["Maturity Level 1", "Maturity Level 2", "Maturity Level 3"]

{{

funnel_chart("Maturity level controls in place",
  {
  'Pass':
    {"Maturity Level 1":5, "Maturity Level 2":7, "Maturity Level 3":12},
  'Fail':
    {"Maturity Level 1":0, "Maturity Level 2":6, "Maturity Level 3":7}
}, labels={'stage': '', 'category': ''}, height=400,
  color_discrete_map={"Pass": "#5e9ca8", 
  "Fail": "#FF4444"}
)

}}

{{

chart(
  px.pie,
  "Risk of vulnerabilities",
  {'Low':3, 'Medium':4, 'High':2},
  names='index',
  color='index',
  values='value',
  height=400,
  color_discrete_map={"Low": "blue", "Medium": "yellow", "High": "red"}
)

}}

{{

chart(
  px.line,
  "Risk of vulnerabilities",
  {
    'Test 1': {'Low':3, 'Medium':4, 'High':2},
    'Test 2': {'Low':6, 'Medium':2, 'High':3}
  },
  x='index',
  y='value',
  color='category',
  height=400,
  labels={"index": "Risk", "value": "Number of vulns"}
)

}}


{{

chart(
  px.bar,
  "Maturity level controls in place",
  {
  'Previous Maturity Levels':
    {"Maturity Level 3":18, "Maturity Level 2":5, "Maturity Level 1":0},
  'Pass':
    {"Maturity Level 1":5, "Maturity Level 2":7, "Maturity Level 3":12},
  'Fail':
    {"Maturity Level 1":0, "Maturity Level 2":6, "Maturity Level 3":7}
  },
  labels={'index': '', 'value': '', 'category': ''},
  height=400, x="value", y="index", color="category",
  color_discrete_map={"Previous Maturity Levels": "#444444", "Pass": "#5e9ca8", 
  "Fail": "#FF4444"},
  orientation='h'
)

}}
