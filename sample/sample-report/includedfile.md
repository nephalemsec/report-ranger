---
special: This is a special variable just for this file!
---
This file has been included into the report body.

Included files have their own scope and can have their own variables defined in the front matter. For example:

* {{special}}

This was generated with the following code:

{% raw %}```
* {{special}}
```{% endraw %}

Yeah it was just one variable.

Including files this way makes building larger reports much simpler. You can push out sections of the report to different people and simply collate them at the end.