# Report Ranger

Report Ranger collects together a bunch of markdown files to create a master markdown file that is suitable to use with Pandoc and Latex to build a PDF file. It solves a lot of problems for you, including:

* Collating your vulnerabilities into sections and severity order. You want to add from the sample vuln repository? Just copy paste it into the folder!
* Automatically building lists of vulnerabilities.
* Automatically putting together document control.
* Allowing full templating of vulnerabilities with variables defined in the markdown headers. You can use a vulnerability template just by changing the headers.
* Collating all the appendices. You want to add a methodology? Just copy paste it into the folder!
* Allowing you to split everything into different files. You can just include them in the template.
* Helper functions for things like tables and IP lists.

# Documentation

The core documentation is in our Gitlab wiki at https://gitlab.com/volkis/report-ranger/-/wikis/home

# Installation

The latest version of the tool can be cloned from Gitlab:

    git clone https://gitlab.com/volkis/report-ranger

It is best to use a pipenv environment to install and run the required packages:

    cd report-ranger
	pipenv install

Note: On MacOS you will need to install some dependencies via HomeBrew

Installing Home Brew:

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

Installing dependencies

    brew install pandoc
    brew install mactex

You will need to close and reopen your terminal window for the mactex installation to complete.

For more information on HomeBrew see: https://brew.sh/

## Nix

If you wish to use the nix package manager to install report ranger, a flake.nix and flake.lock have been provided to automate the installation into an ephemeral shell.

**N.B: this requires flakes to be enabled.**

To use the shell run the following command:

```{nix}
nix develop
```

If you wish you keep current shell formatting the following command will accomodate:

```{nix}
nix develop -c $SHELL
```

# Usage

```
usage: report-ranger.py [-h] [-i INPUT] [-o OUTPUT] [-f FORMAT] [-t TEMPLATE] [-v]

Report Manager collects together a bunch of markdown files to create a master markdown file that is suitable to use
with Pandoc and Latex to build a PDF file.

optional arguments:
  -h, --help            show this help message and exit
  -i INPUT, --input INPUT
                        The main report file to process
  -o OUTPUT, --output OUTPUT
                        The file to output the final markdown to. If the file ends in ".pdf" it will use Pandoc to
                        output a PDF file.
  -f FORMAT, --format FORMAT
                        The format to target (options are "latex"). Default: "latex"
  -t TEMPLATE, --template TEMPLATE
                        The template file. Associated images should be in the same directory.
  -v, --verbose         Turn on verbose mode
```

# Configuration

Because you're unlikely to change the template or format very much, you can edit config.py to fill in the defaults for the variables.

# Markdown headers

Your markdown file should define headers. There are two uses for headers:

* Setting variables for generating the document. See the "header variables" section.
* Calling within the template with Jinja2 signifiers. So {{client}} will return whatever is set in the header for the client variable.

## YAML headers

You can define YAML headers by putting them between '---', '===', or '...'. All three of these will signify yaml. Here's an example of YAML headers:

    ---
	name: My cool vulnerability
	likelihood: High
	impact: Severe
	...
    
	Markdown for the vulnerability
	
## JSON headers

Similarly, you can put headers in JSON within ';;;' in the document. For example:

    ;;;
	{
	"name": "My cool vulnerability",
	"likelihood": "High",
	"impact": "Severe"
	}
	;;;

# Using variables in headers

Report Ranger uses Jinja2 for templating. All the headers are put into the namespace. For example, if your client is Volkis as defined in the header:

    ===
	client: Volkis
	...
	
then this can be called in the document itself:

    We did penetration testing for {{client}}.

The headers of any included file (including vulnerability and appendix files) are local to that file. You can overwrite global variables in your local file. This means that {{likelihood}} will always refer to the likelihood of the current vulnerability file.

## Special headers:

The following are special headers that are processed by Report Ranger:

* **title**: The title of the report
* **version**: Used for document control and the footers
* **vulndir**: The directory of the vulnerabilities. By default this is "vulnerabilities"
* **appendixdir**: The directory of the appendices. By default this is "appendices"
* **ra_methodology**: Setting this to "true" will add a writeup of the risk assessment methodology to the appendix. By default this is true if there are vulnerabilities and false if there aren't.

Vulnerabilities have their own headers:

* **name**: The name of the vulnerability
* **likelihood**
* **impact**

## Special variables

We set a bunch of special variables to make it even easier for you to format everything.

* **list_of_vulnerabilities**: Puts in the markdown for the list of vulnerabilities.
* **document_control**: Puts in the markdown for document control.
* **highestrisk**: This will contain the highest risk in your vulnerabilities. For example, Low, Medium, High, or Critical.
* **cwd**: This allows you to refer to the current markdown file location for screenshots, etc, rather than have the location be relative to the main report file.

## Special functions

* **include_file**: This will get the markdown from the included file and put it in the headers. You usually want to say "env=env" as the second parameter so that global variables are passed through. For example: ```{{ include_file('executivesummary.md', env=env) }}```
* **pathjoin**: An alias for os.path.join. Often used for joining cwd to your screenshot location. For example: ```![Files containing cleartext passwords]({{ pathjoin(cwd,'screenshots/fig.png) }}) {% endif %}```

## Special classes: Output Formatter

The output formatter is set as {{ of }}. It has helper functions that will allow you to dynamically output markdown depending on the target format.

* **of.newsection()**: This will put in any graphics suitable for a new section. In PDFs this could be a title page or fancy graphic.
* **of.table(table, latexdef="", headings=[], colspan=[])**: This will make a new table in the theme colours that matches the target output.
* **of.iplist(listofips, heading="IP list", columns=4)**: This will make a new table in the theme colours that matches the target output.
