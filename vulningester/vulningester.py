import logging
import yaml
import os
import re
import json
from .plugins import WPScanIngester, NessusIngester
from pathlib import Path

plugins = [
    WPScanIngester(),
    NessusIngester()
]

log = logging.getLogger(__name__)


yamlmatch = re.compile(r'^[\.\=\-]{3}$')
jsonmatch = re.compile(r'^\;\;\;$')


def markdown_from_file(file_loc: Path):
    """ Read the markdown from a file.

    Arguments:
    - file_loc: The location of the file to read

    Returns (headers, markdown) where headers is a dict and markdown is a long string """

    log.debug("Reading markdown from file {}".format(file_loc))

    with file_loc.open('r') as vf:
        # If the first line starts with "---" that's our headers We have to make sure that it starts with "---"
        firstline = vf.readline()
        headers = dict()
        if yamlmatch.match(firstline):
            line = vf.readline()
            headerstring = ''
            # Read the headers.
            # Go until we get to the terminator to end the headers
            while not yamlmatch.match(line):
                headerstring += line
                line = vf.readline()
                if not line:
                    raise Exception(
                        f'Markdown file {file_loc} YAML headers don\'t end in a YAML terminator (...,---,===)')
            headers = yaml.safe_load(headerstring)  # Retrieve the YAML headers
            if headers == None:
                headers = {}
            # Convert all keys to lowercase just to make it easier
            headers = {k.lower(): v for k, v in headers.items()}
            markdown = vf.read()  # Read the rest of the file
        elif jsonmatch.match(firstline):
            line = vf.readline()
            headerstring = ''
            # Read the headers.
            # Go until we get to the terminator to end the headers
            while not jsonmatch.match(line):
                headerstring += line
                line = vf.readline()
                if not line:
                    raise Exception(
                        f'Markdown file {file_loc} JSON headers don\'t end in a JSON terminator (;;;)')
            headers = json.loads(headerstring)  # Retrieve the JSON headers
            # Convert all keys to lowercase just to make it easier
            headers = {k.lower(): v for k, v in headers.items()}
            markdown = vf.read()  # Read the rest of the file
        else:
            # There's no headers in this file. Put the first line back and just add it all into the markdown
            markdown = firstline + '\n' + vf.read()

        return headers, markdown


def ingest_vulns(tooloutputs, outputdir: Path, mapperfile: Path, overwrite=False):
    """Ingest the tool output, get vuln writeup locations from the mapper file, and place ingested writeups in the vulndir

    tooloutputs: dict of {name: outputcontent}. name is usually a filename but doesn't have to be. Content is the content as a string.
    outputdir: Where to put the processed markdown files
    mapperfile: The location containing the mapping of plugins to vuln writeups.
    """

    # The format of the data structure for vulnids:
    # {
    #    pluginid: {
    #        vulnid: { 'data': [vulninfo], helpers }
    #    }
    # }
    # pluginid: A string representing the id of the plugin
    # vulnid: A string or integer which can be used to map to vuln writeups
    # vulninfo: A list (often by host) of vuln writeups
    # helpers: Additional info to help in vuln writeups
    vulnids = {}

    # For each tool output
    for name in tooloutputs:
        # Run through each plugin detect routine to see whether they match up
        for plugin in plugins:
            if plugin.detect(tooloutputs[name]):
                # Plugin matches, so lets get the tool output
                log.info("{} detected as matching plugin {}".format(
                    name, plugin.name))

                # Get the plugin output
                pluginoutput = plugin.ingest(tooloutputs[name])
                if plugin.id not in vulnids:
                    vulnids[plugin.id] = {}
                for vuln in pluginoutput:
                    if vuln not in vulnids[plugin.id]:
                        vulnids[plugin.id][vuln] = {
                            'data': pluginoutput[vuln]}
                    else:
                        vulnids[plugin.id][vuln]['data'].append(
                            pluginoutput[vuln])
            else:
                log.info("{} does not match plugin {}".format(
                    name, plugin.name))

    # Afterwards
    # We need the vuln repo dir so we can get the vulnerability files
    vulnrepodir = mapperfile.resolve().parent

    # Get the vuln writeup mapper
    with mapperfile.open('r') as mf:
        mapper = yaml.safe_load(mf)

    # Make sure the output dir is a directory
    if not outputdir.is_dir():
        log.error(
            "Output directory {0} is not a valid directory".format(outputdir))
        raise Exception(
            "Output directory {0} is not a valid directory".format(outputdir))

    for plugin in plugins:
        if plugin.id in vulnids:
            log.info("Outputting plugin {}".format(plugin.name))

            if plugin.id not in mapper:
                log.info("Plugin {} not in mapper. Skipping.".format(plugin.name))
                continue

            # Run through the plugin process function on the final list of outputs
            vulnids[plugin.id] = plugin.process(vulnids[plugin.id])

            for vuln in vulnids[plugin.id]:
                if vuln in mapper[plugin.id]:
                    log.info(f"Vulnerability writeup in mapper for {vuln} from plugin {plugin.name}")
                    headers, markdown = markdown_from_file(vulnrepodir / mapper[plugin.id][vuln])

                    headers[plugin.id] = vulnids[plugin.id][vuln]

                    # Output the YAML code
                    finalpath = outputdir / f"vi-{Path(mapper[plugin.id][vuln]).name}"
                    skip = False
                    if os.path.exists(finalpath):
                        if overwrite:
                            log.warning(f"Overwriting file {finalpath}")
                        else:
                            log.info(f"Skipping file as it already exists: {finalpath}")
                            skip = True

                    if not skip:
                        with open(finalpath, 'w') as vulnfile:
                            vulnfile.write('---\n')
                            vulnfile.write(yaml.dump(headers))
                            vulnfile.write('...\n')
                            vulnfile.write(markdown)


def main(args):
    # Turn on verbose mode
    if args.verbose:
        logging.basicConfig(
            format='%(levelname)s: %(message)s', level=logging.INFO)
    else:
        logging.basicConfig(format='%(levelname)s: %(message)s',
                            level=logging.WARNING)

    tooloutputs = {}

    for tooloutput in args.tooloutputs:
        with open(tooloutput, 'r') as tof:
            tooloutputs[tooloutput] = tof.read()

    ingest_vulns(tooloutputs, args.outputdir,
                 args.mapper, overwrite=args.overwrite)
