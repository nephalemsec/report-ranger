from .plugin_nessus import NessusIngester
from .plugin_wpscan import WPScanIngester

__all__ = ["WPScanIngester", "NessusIngester"]
