import logging
from typing import Dict, Any

log = logging.getLogger(__name__)


class BoilerplateIngester:
    """A boilerplate ingester class for creating new plugins."""

    def __init__(self):
        self.name = 'Boilerplate'  # Sensible name for the file
        self.id = 'boilerplate'  # Used as a key for matching plugins to files
        self.description = 'Boilerplate code used as a base for building new plugins'

    def detect(self, tooloutput: str) -> bool:
        """
        Detect whether the supplied tool output string matches what we're looking for.

        :param tooloutput: The tool output as a string.
        :return: True if the plugin can ingest the tool output, False otherwise.
        """
        return False  # Boilerplate code never matches.

    def ingest(self, tooloutput: str) -> Dict[str, Any]:
        """
        Ingest the tool output.

        :param tooloutput: The tool output as a string.
        :return: A dictionary in the form of {vulnid: data}. The data will be inserted into the front matter of the vuln writeup file.
        """
        return {}
    

    def process(self, vulns: Dict[str, Any]) -> Dict[str, Any]:
        """
        Perform post-processing of markdown files. This function allows you to do helper tables, collate findings, and improve the output.  The reason it is separate from ingest is
        you might be feeding multiple files from the tool with the same vulnerability but for different hosts.

        :param vulns: A dictionary containing the vulnerabilities.
        :return: A dictionary containing the processed vulnerabilities.
        """
        return vulns
